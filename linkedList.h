#ifndef linkedList_H
#define linkedList_H


/* a positive-integer value stack, with no size limit */
typedef struct linkedList
{
	int value;
	linkedList *next ;
}linkedList;

linkedList * addNodeToList(linkedList *head, int x);
linkedList * RemoveNodeFromList(linkedList *head);


#endif 