#include "queue.h"

void initQueue(queue *q, int maxSize)
{
	q->_elements = new unsigned int[maxSize];
	q->_maxSize = maxSize;
	q->_count = 0;
}
void cleanQueue(queue *q)
{
	delete[] q->_elements;
}
void enqueue(queue *q, int newValue)
{
	if (q->_count < q->_maxSize)
	{
		q->_elements[q->_count] = newValue;
		q->_count++;
	}
}
int dequeue(queue *q)
{
	int x,i;
	if (q->_count > 0)
	{
		x = q->_elements[0];
		for ( i = 0; i < q->_count -1; i++)
		{
			q->_elements[i] = q->_elements[i + 1];
		}
		q->_count--;
	}
	else if (q->_count == 0)
	{
		x = -1;
	}
	return x;
}
